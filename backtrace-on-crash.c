#include <stdio.h>
#include <stdlib.h>

#include <windows.h>
#include <dbghelp.h>

static LONG
print_backtrace(EXCEPTION_POINTERS *info)
{
  HANDLE process;
  HANDLE thread;
  CONTEXT *context;
  STACKFRAME64 frame;
  DWORD machine_type;
  DWORD previous_address;

  process = GetCurrentProcess();
  thread = GetCurrentThread();
  context = info->ContextRecord;

  SymSetOptions(SYMOPT_ALLOW_ABSOLUTE_SYMBOLS |
                SYMOPT_ALLOW_ZERO_ADDRESS |
                SYMOPT_AUTO_PUBLICS |
                SYMOPT_DEBUG |
                SYMOPT_DEFERRED_LOADS |
                SYMOPT_LOAD_LINES |
                SYMOPT_NO_PROMPTS);
  if (!SymInitialize(process, NULL, TRUE)) {
    printf("failed to initialize symbol handler\n");
    return EXCEPTION_CONTINUE_SEARCH;
  }

  {
    char path[_MAX_PATH];
    if (SymGetSearchPath(process, path, sizeof(path))) {
      printf("symbol search path: %s\n", path);
    } else {
      printf("failed to get symbol search path\n");
    }
  }

  memset(&frame, 0, sizeof(STACKFRAME64));
  frame.AddrPC.Mode = AddrModeFlat;
  frame.AddrReturn.Mode = AddrModeFlat;
  frame.AddrFrame.Mode = AddrModeFlat;
  frame.AddrStack.Mode = AddrModeFlat;
  frame.AddrBStore.Mode = AddrModeFlat;
#if defined(_M_IX86)
  machine_type = IMAGE_FILE_MACHINE_I386;
  frame.AddrPC.Offset = context->Eip;
  frame.AddrFrame.Offset = context->Ebp;
  frame.AddrStack.Offset = context->Esp;
#elif defined(_M_IA64)
  machine_type = IMAGE_FILE_MACHINE_IA64;
  frame.AddrPC.Offset = context->StIIP;
  frame.AddrStack.Offset = context->IntSP; /* SP is IntSP? */
  frame.AddrBStore.Offset = context->RsBSP;
#elif defined(_M_AMD64)
  machine_type = IMAGE_FILE_MACHINE_AMD64;
  frame.AddrPC.Offset = context->Rip;
  frame.AddrFrame.Offset = context->Rbp;
  frame.AddrStack.Offset = context->Rsp;
#else
#  error "Intel x86, Intel Itanium and x64 are only supported architectures"
#endif

  previous_address = 0;
  while (TRUE) {
    DWORD address;
    IMAGEHLP_MODULE64 module;
    char *buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
    SYMBOL_INFO *symbol = (SYMBOL_INFO *)buffer;
    DWORD line_displacement = 0;
    IMAGEHLP_LINE64 line;

    if (!StackWalk64(machine_type,
                     process,
                     thread,
                     &frame,
                     context,
                     NULL,
                     NULL,
                     NULL,
                     NULL)) {
      printf("done\n");
      break;
    }

    address = frame.AddrPC.Offset;
    printf("%#lx\n", address);
    if (previous_address != 0 && address == previous_address) {
      printf("dup\n");
      break;
    }

    module.SizeOfStruct = sizeof(IMAGEHLP_MODULE64);
    if (SymGetModuleInfo64(process, address, &module)) {
      printf("got module!: <%s>:<%s>\n", module.ModuleName, module.ImageName);
    } else {
      printf("failed module!\n");
    }

    symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    symbol->MaxNameLen = MAX_SYM_NAME;
    if (SymFromAddr(process, address, NULL, symbol)) {
      printf("got name!: <%.*s>\n", (int)symbol->NameLen, symbol->Name);
    } else {
      printf("failed name!\n");
    }

    line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
    if (SymGetLineFromAddr64(process, address, &line_displacement, &line)) {
      printf("got line!: <%s>:%ld:%ld\n"
             , line.FileName, line.LineNumber, line_displacement);
    } else {
      printf("failed line!\n");
    }

    previous_address = address;
  }
  SymCleanup(process);

  return EXCEPTION_CONTINUE_SEARCH;
}

void
a(void)
{
  char *x = NULL;
  x[1000] = 100;
}

static void
b(void)
{
  a();
  return;
}

int
main(void)
{
  SetUnhandledExceptionFilter(print_backtrace);
  b();
  return EXIT_SUCCESS;
}

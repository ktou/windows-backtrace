#include <stdio.h>
#include <stdlib.h>

#include <windows.h>
#include <dbghelp.h>

#ifndef MAXUSHORT
#  define MAXUSHORT 0xffff
#endif

static void
print_backtrace(void)
{
  ULONG frames_to_skip = 0;
  ULONG frames_to_capture = MAXUSHORT;
  void *back_trace[MAXUSHORT];
  USHORT n_frames = CaptureStackBackTrace(frames_to_skip,
                                          frames_to_capture,
                                          back_trace,
                                          NULL);
  if (n_frames == 0) {
    printf("no frames\n");
    return;
  }

  HANDLE process = GetCurrentProcess();
  SymSetOptions(SYMOPT_ALLOW_ABSOLUTE_SYMBOLS |
                SYMOPT_ALLOW_ZERO_ADDRESS |
                SYMOPT_AUTO_PUBLICS |
                SYMOPT_DEBUG |
                SYMOPT_DEFERRED_LOADS |
                SYMOPT_LOAD_LINES |
                SYMOPT_NO_PROMPTS);
  if (!SymInitialize(process, NULL, TRUE)) {
    printf("failed to initialize symbol handler\n");
    return;
  }

  {
    char path[_MAX_PATH];
    if (SymGetSearchPath(process, path, sizeof(path))) {
      printf("symbol search path: %s\n", path);
    } else {
      printf("failed to get symbol search path\n");
    }
  }

  USHORT i;
  for (i = 0; i < n_frames; i++) {
    void *address = back_trace[i];
    IMAGEHLP_MODULE64 module;
    char *buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
    SYMBOL_INFO *symbol = (SYMBOL_INFO *)buffer;
    DWORD line_displacement = 0;
    IMAGEHLP_LINE64 line;

    printf("%p\n", address);

    module.SizeOfStruct = sizeof(IMAGEHLP_MODULE64);
    if (SymGetModuleInfo64(process, (DWORD64)address, &module)) {
      printf("got module!: <%s>:<%s>\n", module.ModuleName, module.ImageName);
    } else {
      printf("failed module!\n");
    }

    symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
    symbol->MaxNameLen = MAX_SYM_NAME;
    if (SymFromAddr(process, (DWORD64)address, NULL, symbol)) {
      printf("got name!: <%.*s>\n", (int)symbol->NameLen, symbol->Name);
    } else {
      printf("failed name!\n");
    }

    line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
    if (SymGetLineFromAddr64(process,
                             (DWORD64)address,
                             &line_displacement,
                             &line)) {
      printf("got line!: <%s>:%ld:%ld\n",
             line.FileName, line.LineNumber, line_displacement);
    } else {
      printf("failed line!\n");
    }
  }
  SymCleanup(process);
}

void
a(void)
{
  print_backtrace();
}

static void
b(void)
{
  a();
  return;
}

int
main(void)
{
  b();
  return EXIT_SUCCESS;
}

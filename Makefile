all: backtrace.exe
all: backtrace-on-crash.exe

clean:
	rm -f *.exe

backtrace.exe: backtrace.c
	x86_64-w64-mingw32-gcc -Wall -g3 -O0 -o $@ $< -l dbghelp

backtrace-on-crash.exe: backtrace-on-crash.c
	x86_64-w64-mingw32-gcc -Wall -g3 -O0 -o $@ $< -l dbghelp
